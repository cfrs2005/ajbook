<?php get_header(); ?>
<div class="single">
	<div class="function clearfix">
		<div class="color">
			<span>配色：</span>
			<a class="bg1"></a>
			<a class="bg2"></a>
			<a class="bg3"></a>
			<a class="bg4"></a>
			<a class="bg5"></a>
			<a class="bg6"></a>
			<a class="bg7"></a>
		</div>
		<div class="font">
			<span>字体：</span>
			<a class="small">小</a>
			<a class="ln">中</a>
			<a class="large">大</a>
		</div>
		<div class="display">
			<a class="wide">宽屏</a>
			<a class="normal">正常</a>
			<a class="narrow">窄屏</a>
		</div>
	</div>

	<script>
		$(document).ready(function(e) {
			var text=$(".article .content").text();
    	var counter=text.length;
			$(".data span").text(counter);
		});
		</script>

    <?php if(have_posts()): ?><?php while(have_posts()):the_post(); ?>		
			
	<div class="article">

		<h1><?php the_title(); ?></h1>
		<div class="data"><?php echo stripslashes(get_option('xs_author')); ?>　更新时间：<?php the_time('y-m-d');?>　字数：<span></span>
		</div>
		<div class="content">
			                  <?php the_content();?>
		
		</div>

	</div>
				<?php endwhile; ?>
	
	<div class="paging clearfix">

		<span>快捷键：←</span>
		<?php previous_post_link('%link','上一章', TRUE)?>
		<a class="home" href="<?php bloginfo(url) ;?>">返回书目</a>
		<?php next_post_link('%link','下一章'); ?>
		<span>快捷键：→</span>
	</div>
	
			<?php else: ?>
			<div class="post">
		<h2><?php _e('Not Found'); ?> </h2>
	</div>
			<?php endif; ?>
	


</div>
<?php get_footer(); ?>