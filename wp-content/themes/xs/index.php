<?php get_header(); ?>

<div class="section">
<h3 class="title">
<div><span><?php bloginfo('name'); ?>目录</span></div>
</h3>
					<div class="fenjuan">

				<?php $display_categories = explode(',', get_option('xs_cat') ); foreach ($display_categories as $category) { ?>

				<?php
					query_posts( array(
						'showposts' =>1,
						'cat' => $category,
						'post__not_in' => $do_not_duplicate
						)
					);
				?>
				
					
						<h4 class="title_fen"><?php single_cat_title(); ?></h4>
									
				  		<?php
							query_posts( array(
								'showposts' =>-1,
								'cat' => $category,
								'offset' => 0,
								'order'=>ASC,
								'post__not_in' => $do_not_duplicate
								)
				 			);
						?>
						<ul class="list clearfix">	
						<?php while (have_posts()) : the_post(); ?>				
							<li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
								<?php echo cut_str($post->post_title,60); ?></a></li>				
						<?php endwhile; ?>
						</ul>						
					<?php wp_reset_query(); ?>
				<?php } ?>			


</div>
</div>
<?php get_footer(); ?>
